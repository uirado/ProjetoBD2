package app.model;
// Generated 30/11/2017 08:40:44 by Hibernate Tools 4.3.1



/**
 * Usuario generated by hbm2java
 */
public class Usuario  implements java.io.Serializable {


     private Integer id;
     private String primeiroNome;
     private String ultimoNome;
     private String login;
     private String senha;
     private String tipo;
     private Administrador administrador;
     private Aluno aluno;
     private Professor professor;

    public Usuario() {
    }

	
    public Usuario(String primeiroNome, String login, String senha, String tipo) {
        this.primeiroNome = primeiroNome;
        this.login = login;
        this.senha = senha;
        this.tipo = tipo;
    }
    public Usuario(String primeiroNome, String ultimoNome, String login, String senha, String tipo, Administrador administrador, Aluno aluno, Professor professor) {
       this.primeiroNome = primeiroNome;
       this.ultimoNome = ultimoNome;
       this.login = login;
       this.senha = senha;
       this.tipo = tipo;
       this.administrador = administrador;
       this.aluno = aluno;
       this.professor = professor;
    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public String getPrimeiroNome() {
        return this.primeiroNome;
    }
    
    public void setPrimeiroNome(String primeiroNome) {
        this.primeiroNome = primeiroNome;
    }
    public String getUltimoNome() {
        return this.ultimoNome;
    }
    
    public void setUltimoNome(String ultimoNome) {
        this.ultimoNome = ultimoNome;
    }
    public String getLogin() {
        return this.login;
    }
    
    public void setLogin(String login) {
        this.login = login;
    }
    public String getSenha() {
        return this.senha;
    }
    
    public void setSenha(String senha) {
        this.senha = senha;
    }
    public String getTipo() {
        return this.tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    public Administrador getAdministrador() {
        return this.administrador;
    }
    
    public void setAdministrador(Administrador administrador) {
        this.administrador = administrador;
    }
    public Aluno getAluno() {
        return this.aluno;
    }
    
    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }
    public Professor getProfessor() {
        return this.professor;
    }
    
    public void setProfessor(Professor professor) {
        this.professor = professor;
    }




}


