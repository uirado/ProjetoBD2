/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.model.dao;

import app.model.Aluno;
import app.model.Usuario;

/**
 *
 * @author uira
 */
public class DaoAluno extends DaoGenerico<Aluno>{
    public DaoAluno(){
        super(Aluno.class);
    }
}
