/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.model.dao;

import app.model.Professor;

/**
 *
 * @author uira
 */
public class DaoProfessor extends DaoGenerico<Professor>{

    public DaoProfessor() {
        super(Professor.class);
    }

}
