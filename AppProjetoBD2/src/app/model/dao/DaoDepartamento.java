/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.model.dao;

import app.model.Departamento;
import app.model.Usuario;
import java.util.List;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

/**
 *
 * @author uira
 */
public class DaoDepartamento extends DaoGenerico<Departamento>{

    public DaoDepartamento() {
        super(Departamento.class);
    }
    
    public Departamento buscaPorNome(String nome){
        Departamento resultado = null;

        SQLQuery query = getSession().createSQLQuery("select * from Departamento where nome='" + nome + "'").addEntity(Departamento.class);
        List<Departamento> lista = query.list();
        
        if(lista.size() == 1){
            resultado = lista.get(0);
        }
        
        return resultado;
    }
 
}
