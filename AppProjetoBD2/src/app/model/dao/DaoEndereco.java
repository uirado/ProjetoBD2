
package app.model.dao;

import app.model.Endereco;


public class DaoEndereco extends DaoGenerico<Endereco>{

    public DaoEndereco() {
        super(Endereco.class);
    }
    
}
