/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.model.dao;

import app.model.Administrador;

/**
 *
 * @author uira
 */
public class DaoAdmin extends DaoGenerico<Administrador>{
    public DaoAdmin(){
        super(Administrador.class);
    }
}
