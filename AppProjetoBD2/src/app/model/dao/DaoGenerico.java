/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.model.dao;

import org.hibernate.Session;
import hibernate.HibernateUtil;
import java.io.Serializable;
import java.util.List;
import java.util.Set;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;

/**
 *
 * @author amanda
 */
public abstract class DaoGenerico<TipoEntidade> {
    
    private Class <TipoEntidade> classeEntidade;
    private SessionFactory factory;
    private Session sessao;
    
    
    protected DaoGenerico(){}
    
    protected DaoGenerico(Class<TipoEntidade> classeEntidade){
        this.classeEntidade = classeEntidade;
        factory = HibernateUtil.getSessionFactory();
        openSession();
    }
    
    public void openSession(){
        sessao = factory.openSession();
    }
    public void closeSession(){
        sessao.close();
    }

    public Exception insert(TipoEntidade obj) {
        sessao.beginTransaction();
        try {
            sessao.save(obj);
            sessao.getTransaction().commit();
        } catch (Exception e) {
            sessao.getTransaction().rollback();
            System.out.println(">>> ERRO: " + e.getCause().getMessage());
            return e;
        }
        return null;
    }
    
    public Exception insert(List<TipoEntidade> list) {
        sessao.beginTransaction();
        try {
            for(TipoEntidade obj : list){
                sessao.save(obj);
            }
            sessao.getTransaction().commit();
        } catch (Exception e) {
            sessao.getTransaction().rollback();
            System.out.println(">>> ERRO: " + e.getCause().getMessage());
            return e;
        }
        return null;
    }
    
    public boolean update(TipoEntidade obj) {
        boolean result = false;
        sessao.beginTransaction();
        
        try {
            sessao.merge(obj);
            sessao.getTransaction().commit();
            result = true;
        } catch (Exception e) {
            sessao.getTransaction().rollback();
            System.out.println(">>> ERRO: " + e.getLocalizedMessage());
        }
        return result;
    }
    
    public boolean update(Set list) {
        boolean result = false;
        sessao.beginTransaction();
        try {
            for(Object obj : list){
                sessao.merge(obj);
            }
            sessao.getTransaction().commit();
            result = true;
        } catch (Exception e) {
            sessao.getTransaction().rollback();
            System.out.println(">>> ERRO: " + e.getLocalizedMessage());
        }
        return result;
    }
    
    public boolean delete(Serializable id) {
        Object obj;
        sessao.beginTransaction();
        try {
            obj = sessao.load(classeEntidade, id);
            if(obj != null){
                sessao.delete(obj);
                sessao.getTransaction().commit();
            } else{
                sessao.getTransaction().rollback();
                System.out.println(">>> ERRO: Objeto não econtrado");
            }
        } catch (Exception e) {
            sessao.getTransaction().rollback();
            System.out.println(">>> ERRO: " + e.getCause().getMessage());
        }
        return false;
    }
    
    public boolean remover(TipoEntidade obj) {
        sessao.beginTransaction();
        try {
            sessao.delete(obj);
            sessao.getTransaction().commit();
        } catch (Exception e) {
            sessao.getTransaction().rollback();
            System.out.println(">>> ERRO: " + e.getCause().getMessage());
        }
        return false;
    }
    
    public TipoEntidade buscar(Serializable id) {
        TipoEntidade resultado;

        resultado = (TipoEntidade) sessao.load(classeEntidade, id);
        
        return resultado;
    }
    
    public List<TipoEntidade> listar(){
        List<TipoEntidade> list = sessao.createCriteria(classeEntidade).list();
        return list;
    }
    
    protected SessionFactory getSessionFactory(){
        return factory;
    }
    
    protected Session getSession(){
        return sessao;
    }
}
