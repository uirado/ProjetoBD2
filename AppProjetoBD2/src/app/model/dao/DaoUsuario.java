/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.model.dao;

import Util.Seguranca;
import app.model.Administrador;
import app.model.Aluno;
import app.model.Endereco;
import app.model.Matricula;
import app.model.Professor;
import app.model.Usuario;
import java.util.List;
import java.util.Set;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
/**
 *
 * @author amanda
 */
public class DaoUsuario extends DaoGenerico<Usuario>{
    public DaoUsuario(){
        super(Usuario.class);
    }
    
    public Usuario buscaPorLogin(String login){
        Usuario resultado = null;
        SQLQuery query = getSession().createSQLQuery("select * from Usuario where login='" + login + "'").addEntity(Usuario.class);
        List<Usuario> lista = query.list();
        
        if(lista.size() == 1){
            resultado = lista.get(0);
        }
        
        return resultado;
    }

    @Override
    public Exception insert(Usuario user) {        
        String senha = Seguranca.MD5(user.getSenha());
        user.setSenha(senha);
        return super.insert(user); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean remover(Usuario user) {
        
        Session sessao = getSession();
        sessao.beginTransaction();
        
        try {
            Aluno aluno = user.getAluno();
            Professor prof = user.getProfessor();
            Administrador admin = user.getAdministrador();
        
            if(aluno != null){
                Endereco end = aluno.getEndereco();
                Set<Matricula> mat = aluno.getMatriculas();
                if(end != null){
                    sessao.delete(end);
                }
                if(!mat.isEmpty()){
                    for(Matricula m : mat){
                        sessao.delete(m);
                    }
                }
                sessao.delete(aluno);
            }
            if(prof != null){
                sessao.delete(prof);
            }
            if(admin != null){
                sessao.delete(admin);
            }
            sessao.getTransaction().commit();
            return super.remover(user);
        } catch (Exception e) {
            sessao.getTransaction().rollback();
            System.out.println("Não foi possível remover o usuário, verifique a dependência");   
            System.out.println(e.getCause().getMessage());
            return false;
        }
    }
    
    
    
}
