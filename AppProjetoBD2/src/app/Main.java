/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import app.model.Aluno;
import app.model.AlunosInscritos;
import app.model.DisciplinaCursoId;
import app.model.Historico;
import app.model.HistoricoId;
import app.model.Periodo;
import app.model.PeriodoId;
import app.model.Boletim;
import app.model.BoletimId;
import app.model.DisciplinaCurso;
import app.model.Professor;
import app.model.Turma;
import app.model.Usuario;
import app.model.Departamento;
import hibernate.HibernateUtil;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;


public class Main{
    
    private static Session session;
    public static void main(String[] args) {
        
        
        
        session = HibernateUtil.getSessionFactory().openSession();
        
        criarTurmas();
        exemploCadastros();
        Professor silvio = (Professor) session.load(Professor.class, 123);
        trocarCoordenador(silvio);
     
        
        session.close();
        HibernateUtil.getSessionFactory().close();
    }
    
    
    public static void trocarCoordenador(Professor novoCoord){
        Departamento dpto = novoCoord.getDepartamento();
        Professor antigoCoord;
        session.beginTransaction();
        try {
            SQLQuery query = session.createSQLQuery("select * from Professor where idDpto =" +dpto.getIdDpto()+ " and coord=1").addEntity(Professor.class);
            antigoCoord = (Professor) query.list().get(0);
            antigoCoord.setCoord(false);
            session.update(antigoCoord);
            session.flush();
            novoCoord.setCoord(true);
            session.update(novoCoord);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            System.out.println("Erro ao fazer troca de coordenador");
            System.out.println(e.getCause().getMessage());
        }
        
    }
    
    public static void criarTurmas(){
        //processo de criação de turmas
        int totalInscritos, ano, semestre, idCurso, idAluno, idDisci, turmasNecessarias, alunosPorTurma;
        Aluno aluno;
        DisciplinaCurso disciCurso;
        DisciplinaCursoId idDisciCurso = new DisciplinaCursoId();
        AlunosInscritos registro;
        Turma turma;
        Periodo periodo;
        Boletim boletim;
        Historico historico;
        
        Query queryInscricoes = session.createSQLQuery("CALL getIncricoesAtuais()");
        
        List<Object[]> cursoDisciEscolhidas = queryInscricoes.list();
        List<Object> inscritos;
        
        for(int i = 0; i < cursoDisciEscolhidas.size(); i++){
            //System.out.println("idCurso: " + disciEscolhidas.get(i)[0]+ ", idDisci: " + disciEscolhidas.get(i)[1]);
            ano = (int) cursoDisciEscolhidas.get(i)[0];
            semestre = (int) cursoDisciEscolhidas.get(i)[1];
            idCurso = (int) cursoDisciEscolhidas.get(i)[2];
            idDisci = (int) cursoDisciEscolhidas.get(i)[3];
            idDisciCurso.setIdCurso(idCurso);
            idDisciCurso.setIdDisci(idDisci);
            
            Query queryInscritos = session.createSQLQuery("CALL alunosInscritosDisciplinasCurso (" + ano + "," + semestre + "," + idCurso + "," + idDisci + ")").addEntity(AlunosInscritos.class);
            inscritos = queryInscritos.list();
            totalInscritos = inscritos.size();
            
            disciCurso = (DisciplinaCurso) session.load(DisciplinaCurso.class, idDisciCurso);

            if(totalInscritos < 10){
                System.out.println("Alunos insuficientes para formar turma.");
                System.out.println("Curso: " + disciCurso.getCurso().getNome() + " | Disciplina: " + disciCurso.getDisciplina().getNome() + " | Inscritos: " + totalInscritos);
            } else{
                int resto;
                turmasNecessarias = totalInscritos/30 + 1;
                alunosPorTurma = totalInscritos/turmasNecessarias;
                
                for(int j = 0; j < turmasNecessarias; j++){
                    
                    turma = new Turma();
                    turma.setDisciplinaCurso(disciCurso);
                    
                    //transaction abre e fecha a cada turma criada com sucesso
                    session.beginTransaction();
                    try {
                        session.save(turma);
                        for(int k = 0; k < alunosPorTurma; k++){
                            registro = (AlunosInscritos) inscritos.get(0);
                            registro.getPeriodoInscricao().getStatus();

                            aluno = registro.getAluno();
                            idAluno = aluno.getIdAluno();
                            
                            historico = (Historico) session.load(Historico.class, new HistoricoId(idCurso, idAluno));
                            
                            boletim = new Boletim(new BoletimId(idAluno, turma.getIdTurma()), aluno, turma);
                            periodo = new Periodo(new PeriodoId(idCurso, idAluno, turma.getIdTurma()), historico, turma);
                            session.save(periodo);
                            session.save(boletim);
                            
                            //apagar registro da tabela Inscricoes Alunos e da lista de inscritos
                            session.delete(registro);
                            inscritos.remove(0);
                        }
                        //caso a divisão tenha resto, os alunos que sobram ficam na última turma formada.
                        if(j+1 == turmasNecessarias){ //caso seja a ultima turma
                            resto = inscritos.size();
                            for(int k = 0; k < resto; k++){
                                registro = (AlunosInscritos) inscritos.get(0);
                                registro.getPeriodoInscricao().getStatus();

                                aluno = registro.getAluno();
                                idAluno = aluno.getIdAluno();

                                historico = (Historico) session.load(Historico.class, new HistoricoId(idCurso, idAluno));

                                boletim = new Boletim(new BoletimId(idAluno, turma.getIdTurma()), aluno, turma);
                                periodo = new Periodo(new PeriodoId(idCurso, idAluno, turma.getIdTurma()), historico, turma);
                                session.save(periodo);
                                session.save(boletim);

                                //apagar registro da tabela Inscricoes Alunos e da lista de inscritos
                                session.delete(registro);
                                inscritos.remove(0);
                            }
                        }
                        session.getTransaction().commit();
                    } catch (Exception e) {
                        session.getTransaction().rollback();
                        System.out.println("Erro ao criar turma.");
                        System.out.println(e.getCause().getMessage());
                    }
                    
                    
                }
            }
        }
    }

    private static void exemploCadastros() {
        session.beginTransaction();
        try {
            Departamento dpto = new Departamento("CCT", "exatas");
            Usuario user1 = new Usuario("Fernando", "fernando", "123", "professor");
            Professor prof1 = new Professor(dpto, user1, true);
            
            Usuario user2 = new Usuario("Silvio", "silvio", "123", "professor");
            Professor prof2 = new Professor(dpto, user2, false);
            
            prof1.setCfe("12345678912");
            prof2.setCfe("65498732144");
            dpto.getProfessors().add(prof1);
            dpto.getProfessors().add(prof2);

            session.save(dpto);
            session.save(user1);
            session.save(prof1);
            session.save(user2);
            session.save(prof2);
            session.getTransaction().commit();

        } catch (Exception e) {
            session.getTransaction().rollback();
            System.out.println(e.getCause().getMessage());
        }
    }
    
}
