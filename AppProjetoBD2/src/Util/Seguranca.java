/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import app.model.dao.DaoUsuario;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author amanda
 */
public class Seguranca {
    
    
    public static String MD5(String text) {
        MessageDigest m = null;
        try {
            m = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(DaoUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        m.update(text.getBytes());
        byte[] hashMd5 = m.digest();
        
        //text = hashMd5.toString();

        return text;
    }
    
    public static boolean checkMD5(String text, String md5){
        String t = MD5(text);
        if(t.equals(md5)){
            return true;
        } else{
            return false;
        }
    }
}
