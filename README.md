Universidade Católica de Pernambuco

Banco de Dados II - 2017.2 noite

Prof. Fernando Wanderley

## Trabalho de Graduação em Banco de Dados

Grupo: Uirá Veríssimo, Paulo Bittencourt e Sérgio Silveira

#### Dada a descrição do seguinte Sistema Acadêmico:
- A  gerência  acadêmica  mantém um  controle de  alunos,  cursos,  disciplinas,  turmas,  professores  e  do histórico escolar dos alunos.
- Os alunos são admitidos em um curso através de vestibular ou transferência e um aluno só pode estar matriculado em um curso, em um dado instante.
- Os alunos, quando ingressam na  universidade,  preenchem  uma  ficha  de  matricula,  com nome  e  endereço  e  recebem  um  número  de matricula  no  curso.
- A universidade  oferece  disciplinas  que  devem  ter  no  mínimo  10  alunos  para  que  possam  ser  oferecidas.
- Para  que  a  qualidade  do  ensino  não  seja  prejudicada,  cada  disciplina  somente pode ter até 30 alunos inscritos.
- Os cursos são compostos por disciplinas que podem ser obrigatórias ou optativas,  dependendo  do  curso  a  que  pertencem.
- Cada  disciplina  está  sob  responsabilidade  de  um departamento.
- Cada disciplina é lecionada por um professor.
- Os professores podem ser cadastrados sem estar  lecionando  uma  disciplina.
- Cada  professor  possui  nome, número de  registro  do  CFE  (conselho federal  de  educação)  e  está  vinculado  a  um  departamento. 
- Para  permitir  o  acompanhamento  do desempenho dos alunos a universidade mantem um histórico escolar para cada aluno.
- O histórico possui a relação de disciplinas cursadas pelo aluno em toda a sua vida acadêmica durante o curso, contendo para cada disciplina, a nota final e data em que a disciplina foi cursada.
- Os departamentos são responsáveis pelos  cursos  de  suas  áreas  de  atuação.
- A responsabilidade do departamento consiste em definir o número de créditos exigidos para a conclusão do curso, a carga horário do curso e o número total de horas nas disciplinas obrigatórias.
- A universidade adota um sistema progressivo de aprendizado, no qual cada disciplina possui pré-requisitos em relação à outras disciplinas, sendo que uma disciplina pode ter no máximo 3 disciplinas como pré-requisito e pode não ter nenhuma disciplina como pré-requisito.
- Um aluno pode se  inscrever em até 7 disciplinas por período, mas também pode não estar inscrito em nenhuma disciplina em um período, caracterizando um trancamento de matricula do curso.
- O aluno pode se inscrever somente 3 vezes na mesma disciplina, ou seja, não pode cursar a mesma disciplina mais de 3 vezes.

#### O seu trabalho será composto em duas partes:
1. Elaborar um Documento de Especificação do Banco (3,0 pontos).
2. Implementar um Sistema de Informação (7,0 pontos).

#### Quanto ao Documento de Especificação, seguem as seguintes seções exigidas:
1. Capa;
2. Descrição do Domínio;
3. Modelo Conceitual (atributos, relacionamentos, heranças);
4. Mapeamento Relacional (verifique todas as restrições de integridade relacional);
5. Análise das Regras de Normalização (quando aplicável);
6. Modelo de Dados (com todas as regras de checagem semântica possível);
7. Stored Procedures (definidas durantes o projeto);
8. Triggers (definidas durante o projeto);
9. Segurança (defina estratégias de segurança para seu banco)

#### Quanto ao Sistema de Informação pede-se os seguintes requisitos:
1. Desenvolva uma aplicação (Desktop ou Web) através de sua linguagem de preferência;
2. Implemente soluções que realize 3 (três) consultas para cada RIGHT, LEFT e INNER JOIN; (OBS.: Explore todas as funções de agregação de SQL, Group By, Having, Exists, SubQuery, Like);
3. Implemente soluções que sua aplicação "consuma" 5 (Stored Procedures);
4. Implemente soluções que sua aplicação "consuma" 5 (Triggers);
5. Escolha um framework de Mapeamento Objeto Relacional (de acordo com sua linguagem) para mapear todas as suas classes para a base de dados relacional (Ex.: Hibernate para Java); (Realize testes pontuais de controle –INSERIR, REMOVER, ATUALIZAR e CONSULTAR várias entidades pelo framework, após o resultado de mapeamento);
6. Defina 4 (quatro) mecanismos de controle transacional da sua aplicação que serão gerenciados pelo framework de mapeamento objeto relacional;