-- SQL para criacao e uso do esquema
drop database if exists projetobd2;

create database `projetobd2`;
use projetobd2;

create table Usuario (
  id int auto_increment,
  primeiroNome varchar (64) NOT NULL,
  ultimoNome varchar (64),
  login varchar (32) NOT NULL unique,
  senha varchar (32) NOT NULL,
  tipo enum ('administrador', 'aluno', 'professor') NOT NULL,
  primary Key (id)
); 

create table Administrador (
  idAdmin int,
  primary key (idAdmin),
  foreign key (idAdmin) references Usuario(id)
);

create table Departamento (
  idDpto int auto_increment,
  nome varchar (64) NOT NULL unique,
  areaAtuacao enum ('humanas', 'exatas', 'saúde') NOT NULL,
  primary key (idDpto)
); 

create table Professor (
  idProf int,
  cfe char (11) unique,
  idDpto int NOT NULL,
  coord bool NOT NULL,
  primary key (idProf),
  foreign key (idProf) references Usuario(id),
  foreign key (idDpto) references Departamento(idDpto)
); 

create table Aluno (
  idAluno int,
  cpf char(11) unique,
  primary key (idAluno),
  foreign key (idAluno) references Usuario(id)
);

create table Endereco (
  idAluno int,
  logradouro varchar (64) NOT NULL,
  numero varchar (5) NOT NULL,
  complemento varchar(128),
  bairro varchar (32) NOT NULL,
  cidade varchar (32) NOT NULL, 
  estado varchar (32) NOT NULL,
  primary key (idAluno),
  foreign key (idAluno) references Aluno(idAluno)
); 

create table Curso (
  idCurso int auto_increment,
  nome varchar(32) NOT NULL,
  cargaHorariaMinima int NOT NULL,
  horasObrigatorias int default 0,
  cargaHorariaTotal int default 0,
  creditos int NOT NULL DEFAULT 0,
  status enum ('horas insuficientes', 'disponível') default 'horas insuficientes',
  idDpto int NOT NULL,
  primary key (idCurso),
  foreign key (idDpto) references Departamento(idDpto),
  check (creditos >= 0 and horasObrigatoriias >= 0 and cargaHorariaMinima >=0 and cargaHorariaTotal >= 0)
); 

create table Matricula (
  idCurso int,
  idAluno int,
  numero char(12) unique,
  status enum ('ativa', 'trancada', 'concluida', 'cancelada') default 'ativa',
  tipoAdmissao enum ('vestibular','transferencia') NOT NULL,
  data date,
  primary key (idCurso, idAluno),
  foreign key (idCurso) references Curso(idCurso),
  foreign key (idAluno) references Aluno (idAluno)
); 



create table Disciplina (
  idDisci int auto_increment,
  nome varchar (32) NOT NULL,
  idDpto int,
  idProf int,
  primary key (idDisci),
  foreign key (idDpto) references Departamento(idDpto),
  foreign key (idProf) references Professor (idProf)
); 

create table DisciplinaCurso (

  idCurso int,
  idDisci int,
  periodo int,
  obrigatoriedade bool NOT NULL,
  cargaHoraria int NOT NULL,
  creditos int NOT NULL,
  primary key (idCurso, idDisci),
  foreign key (idCurso) references Curso (idCurso),
  foreign key (idDisci) references Disciplina (idDisci),
  check (cargaHoraria >= 0 and creditos >= 0 and periodo > 0)
);

create table PreRequisito (
  idCurso int not NULL,
  idDisci int not NULL,
  idCursoPreReq int not NULL,
  idDisciPreReq int not NULL,
  primary key(idCurso, idDisci, idDisciPreReq),
  foreign key (idCurso, idDisci) references DisciplinaCurso (idCurso, idDisci),
  foreign key (idCursoPreReq, idDisciPreReq) references DisciplinaCurso (idCurso, idDisci)
); 

create table PeriodoInscricao (
    ano int,
    semestre int,
    status enum ('ativo','encerrado') not null,
    primary key (ano, semestre),
    check (semestre in (1,2))
);

create table AlunosInscritos (
	ano int,
    semestre int,
	idAluno int,
    idCurso int,
    idDisci int,
    data datetime,
    primary key (ano, semestre, idAluno, idCurso, idDisci),
    foreign key (ano, semestre) references PeriodoInscricao (ano, semestre),
    foreign key (idAluno) references Aluno (idAluno),
    foreign key (idCurso, idDisci) references DisciplinaCurso (idCurso, idDisci)
);

create table Historico (
  idCurso int,
  idAluno int,
  media float,
  horasCursadas int,
  primary key (idCurso, idAluno),
  foreign key (idCurso, idAluno) references Matricula (idCurso, idAluno),
  check (media >= 0 and media <= 10 and horasCursadas >= 0)
); 


create table Turma (
  idTurma int auto_increment,
  codigo varchar (16) null unique,
  idCurso int,
  idDisci int,
  primary key (idTurma),
  foreign key (idCurso, idDisci) references DisciplinaCurso (idCurso, idDisci)
);


-- trigger para inserir status em turma

create table Periodo (
  idCurso int,
  idAluno int,
  idTurma int,
  ano int,
  semestre int,
  numero int,
  status enum ('corrente', 'encerrado') default 'corrente',
  primary key (idCurso, idAluno, idTurma),
  foreign key (idCurso, idAluno) references Historico(idCurso, idAluno),
  foreign key (ano, semestre) references PeriodoInscricao (ano, semestre),
  foreign key (idTurma) references Turma(idTurma),
  check (numero > 0)
); 
					
create table Boletim (
  idAluno int,
  idTurma int,
  primeiraNota float,
  segundaNota float,
  notaProvaFinal float,
  media float,
  faltas int DEFAULT 0,
  situacao enum ('pendente','aprovado por media','aprovado','reprovado por media','reprovado','reprovado por falta') default 'pendente',
  primary key (idAluno, idTurma),
  foreign key(idAluno) references Aluno (idAluno),
  foreign key (idTurma) references Turma(idTurma),
  check (primeiraNota >= 0 and primeiraNota <= 10 and segundaNota >= 0 and segundaNota <= 10 and notaProvaFinal >= 0 and notaProvaFinal <= 10 and faltas >= 0)
);

create table debug(
	msg varchar (256)
);

drop user if exists 'admin'@'localhost';
create user 'admin'@'localhost' identified by '0';

drop user if exists 'aluno'@'localhost';
create user 'aluno'@'localhost' identified by '0';

drop user if exists 'professor'@'localhost';
create user 'professor'@'localhost' identified by '0';

drop user if exists 'coordenador'@'localhost';
create user 'coordenador'@'localhost' identified by '0';

