
DROP TRIGGER IF EXISTS novaMatricula;
DROP TRIGGER IF EXISTS gerarHistorico;
DROP TRIGGER IF EXISTS setCodigoTurma;
DROP TRIGGER IF EXISTS atualizarBoletim;
DROP TRIGGER IF EXISTS novoBoletim;
DROP TRIGGER IF EXISTS setDataInscricao;
DROP TRIGGER IF EXISTS atualizaCursoINSERT;
DROP TRIGGER IF EXISTS atualizaCursoUPDATE;
DROP TRIGGER IF EXISTS validaPreRequisito;
DROP TRIGGER IF EXISTS criptografarSenha;
DROP TRIGGER IF EXISTS coordDeptoINSERT;
DROP TRIGGER IF EXISTS coordDeptoUPDATE;
DROP TRIGGER IF EXISTS maxInscricoes;
DROP TRIGGER IF EXISTS novoPeriodo;


﻿-- Trigger para criar o número de matricula (data)
DELIMITER $
CREATE TRIGGER novaMatricula BEFORE INSERT
ON Matricula
FOR EACH ROW
BEGIN
    -- verifica se o aluno já tem uma matrícula ativa ou se o aluno já tem uma matrícula existente no mesmo curso, caso sim, anula o insert
    SET @matriculaAtiva = (select (count(*) > 0) from Matricula m where m.idAluno = NEW.idAluno and m.status = 'ativa');
    SET @matriculaExistente = (select (count(*) > 0) from Matricula m where m.idAluno = NEW.idAluno and m.idCurso = NEW.idCurso);
    
    if(@matriculaAtiva > 0) then
        signal sqlstate '45000' set message_text = 'O aluno já possui uma matrícula ativa';
	elseif (@matriculaExistente) then
		set @statusMat = (select `status` from matricula where idAluno = NEW.idAluno and idCurso = NEW.idCurso);
        set @msg = concat('O aluno já está matriculado no curso escolhido, com status ', @statusMat);
		signal sqlstate '45000' set message_text = @msg;
    else
		-- caso não tenha problemas de matricula repetida, continua a inserção e calcula o numero de matricula
    
		/*digito = ((ano + semestre*2 + idCurso + idAluno*2) mod 10 */
		SET @digito = ((year(curdate()) + ceil(month(curdate())/6)*2 + NEW.idCurso + NEW.idAluno*2) % 11) % 10;
		/*concatena (ano + semestre + (idCurso mod 100) + (idAluno mod 100) + digito*/
		/* a função LPAD adiciona zeros à esquerda do número para ficar sempre com dois dígitos, por exemplo: 5 -> 05 */
		SET @numero = concat((year(curdate())), ceil(month(curdate())/6), LPAD( NEW.idCurso % 100, 2, '0'), LPAD( NEW.idAluno % 100, 2, '0'), "-", @digito);
		
		SET NEW.numero = @numero, NEW.data = curdate();
    end if;
END$
DELIMITER ;


﻿-- Trigger para criar o histórico
DELIMITER $
CREATE TRIGGER gerarHistorico AFTER INSERT
ON Matricula
FOR EACH ROW
BEGIN
    insert into Historico (idCurso, idAluno, horasCursadas)
	values (NEW.idCurso, NEW.idAluno, 0);
END$
DELIMITER ;


-- trigger para inserir os dados que faltam em periodo
DELIMITER $
CREATE TRIGGER novoPeriodo before INSERT
ON Periodo
FOR EACH ROW
BEGIN
	if(NEW.ano is null and NEW.semestre is null) then
		set @ano = (select ano from PeriodoInscricao where status = 'ativo');
		set @semestre = (select semestre from PeriodoInscricao where status = 'ativo');
    end if;
    
    if(NEW.numero is null) then
		-- calcula o numero do periodo atual, caso seja o primeiro ou seguindo a numeração do ultimo
		set @numero = (select coalesce(max(numero),0) from Periodo
			where ano = @ano and semestre = @semestre and idCurso = NEW.idCurso and idAluno = NEW.idAluno);
	
		if(@numero = 0) then
			set @numero = 1;
		end if;
	else
        set @numero = NEW.numero;
    end if;
    
    set NEW.ano = @ano;
    set NEW.semestre = @semestre;
    set NEW.numero = @numero;
    set NEW.status = 'corrente';
END$
DELIMITER ;


﻿-- Trigger para criar o codigo da turma
DELIMITER $
CREATE TRIGGER setCodigoTurma BEFORE INSERT
ON Turma
FOR EACH ROW
BEGIN
    SET @id = (select auto_increment from information_schema.tables where table_name = 'Turma' and table_schema = database());
	SET @inicialCurso = (select LEFT(c.nome,1) from Curso c where c.idCurso = NEW.idCurso);
    SET @inicialDisci = (select LEFT(d.nome,1) from Disciplina d where d.idDisci = NEW.idDisci);
    SET @lastId = (select coalesce(max(idTurma), 0) from Turma);
	SET @cod = concat(@inicialCurso, @inicialDisci, '-', @lastId+1);
    
	SET NEW.codigo = @cod;
END$
DELIMITER ;


-- trigger para atualizar a situação do aluno na turma em relação à suas notas
DELIMITER $
CREATE TRIGGER atualizarBoletim BEFORE UPDATE
ON Boletim
FOR EACH ROW
BEGIN
/*corpo do código*/
 if (new.faltas <=18) then
	if (new.notaProvaFinal IS NOT NULL) then
		set @media=((new.media+new.notaProvaFinal)/2);
        set new.media= @media;
		if(@media>=5) then
			set new.situacao= 'aprovado';
		else
			set new.situacao= 'reprovado';
		end if;
    elseif (new.segundaNota IS NOT NULL) then
		set @media=(new.primeiraNota+new.segundaNota)/2;
        set new.media= @media;
		if (@media>=7) then
		    set new.situacao= 'aprovado por media';
		elseif (@media<3)then
            set new.situacao= 'reprovado por media';
        end if;
    else
		set new.situacao='pendente';
    end if;
else
    set new.situacao='reprovado por falta';
end if;         

END $
DELIMITER ;


-- atribui valores padões para novos boletins
DELIMITER $
CREATE TRIGGER novoBoletim BEFORE INSERT
ON Boletim
FOR EACH ROW
BEGIN
/*corpo do código*/
	set NEW.faltas = 0;
    set NEW.situacao = 'pendente';
END $
DELIMITER ;


-- insere a data no momento da inscrição
DELIMITER $
CREATE TRIGGER setDataInscricao BEFORE INSERT
ON AlunosInscritos
FOR EACH ROW
BEGIN
/*corpo do código*/
	set NEW.data = now();
END $
DELIMITER ;


-- triggers para atualizar as informações do curso depois de atribuir ou modificar uma disciplina associada ao curso.
DELIMITER $
create trigger atualizaCursoINSERT after insert
on DisciplinaCurso
for each row
begin
	call atualizaCurso(new.idCurso);
end $
DELIMITER ;
DELIMITER $
create trigger atualizaCursoUPDATE after update
on DisciplinaCurso
for each row
begin
	call atualizaCurso(new.idCurso);
end $
DELIMITER ;


-- trigger para impedir que uma disciplina tenha mais de 3 pre requisitos ou que ele seja pre requisito dela mesma
DELIMITER $
create trigger validaPreRequisito before insert
on PreRequisito
for each row
begin
	-- a primary key composta evita pre requisitos ciclicos (ex: 1,2 e 2,1)
    
    -- idCurso e idCursoPreReq são iguais
    set NEW.idCursoPreReq = NEW.idCurso;

    set @qtdPreReq = (select count(*) from PreRequisito where idCurso = NEW.idCurso and idDisci = NEW.idDisci);
    
    -- uma disciplinão não pode ter ela mesma como pré requisito, nem ter mais de 3 pré requisitos no mesmo curso
    if(NEW.idDisci = NEW.idDisciPreReq) then
		signal sqlstate '45000' set message_text = 'Uma disciplina não pode ter ela mesma como pré-requisito';
    elseif (@qtdPreReq = 3) then
        signal sqlstate '45000' set message_text = 'Uma disciplina só pode ter até três pré-requisitos';
    end if;
end $
DELIMITER ;


DELIMITER $
create trigger criptografarSenha before insert
on Usuario
for each row
begin
	-- set new.senha = AES_ENCRYPT(new.senha, 'chave');
    -- para decriptar
    -- CAST(AES_DECRYPT(senha, 'chave') as char)
end $

DELIMITER ;


-- evita que um departamento tenha mais de um coordenador INSERT
DELIMITER $
create trigger coordDeptoINSERT before insert
on Professor
for each row
begin
	-- se já existir um coordenador no departamento o insert é cancelado
	if(NEW.coord = 1) then
		set @existeCoord = (select count(*) > 0 from Professor p where idDpto = NEW.idDpto and p.coord = 1);
        if(@existeCoord) then
            signal sqlstate '45000' set message_text = 'O departamento já tem coordenador';
        end if;
    end if;
end $
DELIMITER ;


-- evita que um departamento tenha mais de um coordenador UPDATE
DELIMITER $
create trigger coordDeptoUPDATE before update
on Professor
for each row
begin
	-- se já existir um coordenador no departamento o insert é cancelado
	if(NEW.coord = 1) then
		set @existeCoord = (select count(*) > 0 from Professor p where idDpto = NEW.idDpto and p.coord = 1);
        if(@existeCoord) then
            signal sqlstate '45000' set message_text = 'O departamento já tem coordenador';
        end if;
    end if;
end $
DELIMITER ;


DELIMITER $
create trigger validaInscricao before insert
on AlunosInscritos
for each row
begin
	-- verificar se o periodo de inscricao esta ativo
    set @periodoInscricaoAtivo = (select count(*) > 0 from PeriodoInscricao where status = 'ativo');
    if(@periodoInscricaoAtivo) then
		signal sqlstate '45000' set message_text = 'O período de inscrição não está ativo';
    end if;

    -- verifica se a matricula do aluno está ativa
	set @matriculaAtiva = (select (count(*) > 0) from Matricula
		where idAluno = NEW.idAluno and idCurso = NEW.idCurso and status = 'ativa');
    if(@matriculaAtiva = 0) then
		set @status = select status from Matricula where idAluno = NEW.idAluno and idCurso = NEW.idCurso;
		set @msg = concat('A matrícula do aluno está ', @status);
		signal sqlstate '45000' set message_text = @msg;
    end if;
    
    -- impede que o aluno se inscreva em mais de 7 disciplinas
    set @qtdInscricoes = (select count(*) from AlunosInscritos
		where ano = NEW.ano and semestre = new.semestre and idAluno = NEW.idAluno and idCurso = NEW.idCurso);
	if(@qtdInscricoes >= 7) then
		signal sqlstate '45000' set message_text = 'Só é possível se inscrever em no máximo 7 disciplinas';
    end if;
    
    -- lista as turmas que o aluno já cursou a disciplina
    set turmas = SELECT 	t.idTurma, b.situacao
					FROM 	Boletim b,
							Turma t
					WHERE 	b.idAluno = new.idAluno
							AND b.idTurma = t.idTurma
							AND t.idCurso = new.idCurso
							AND t.idDisci = new.idDisci;
	set @qtd = select count(*) from turmas;
	set @aprovado = (select count(*) > 0 from turmas where situacao = 'aprovado' or situacao = 'aprovado por media');
    
    -- verifica se o aluno já foi aprovado na disciplina
    if(@aprovado) then
		signal sqlstate '45000' set message_text = 'O aluno já concluiu esta disciplina';
	end if;
    -- verifica se o aluno já cursou a disciplina 3 vezes
    if(@qtd >= 3) then
		signal sqlstate '45000' set message_text = 'O não pode cursar a disciplina mais que três vezes';
	end if;
end ;
DELIMITER ;



