-- todos os privilégios para administrador
grant all privileges on projetobd2.* to 'admin'@'localhost';

-- privilégios de aluno
grant insert on projetobd2.AlunosInscritos to 'aluno'@'localhost';
grant select on projetobd2.AlunosInscritos to 'aluno'@'localhost';
grant update on projetobd2.AlunosInscritos to 'aluno'@'localhost';
grant delete on projetobd2.AlunosInscritos to 'aluno'@'localhost';

revoke insert on projetobd2.AlunosInscritos from 'aluno'@'localhost';
revoke select on projetobd2.AlunosInscritos from 'aluno'@'localhost';
revoke update on projetobd2.AlunosInscritos from 'aluno'@'localhost';
revoke delete on projetobd2.AlunosInscritos from 'aluno'@'localhost';

grant select on projetobd2.Curso to 'aluno'@'localhost';
grant select on projetobd2.DisciplinaCurso to 'aluno'@'localhost';
grant select on projetobd2.Disciplina to 'aluno'@'localhost';
grant select on projetobd2.Prerequisito to 'professor'@'localhost';
grant select on projetobd2.Matricula to 'aluno'@'localhost';
grant select on projetobd2.Historico to 'aluno'@'localhost';
grant select on projetobd2.Periodo to 'aluno'@'localhost';
grant select on projetobd2.Turma to 'aluno'@'localhost';
grant select on projetobd2.Boletim to 'aluno'@'localhost';

grant select on projetobd2.Endereco to 'aluno'@'localhost';
grant update on projetobd2.Endereco to 'aluno'@'localhost';

grant select on projetobd2.Aluno to 'aluno'@'localhost';
grant update on projetobd2.Aluno to 'aluno'@'localhost';

grant select on projetobd2.Usuario to 'aluno'@'localhost';
grant update on projetobd2.Usuario to 'aluno'@'localhost';



-- privilégios de professor
grant select on projetobd2.Curso to 'professor'@'localhost';
grant select on projetobd2.DisciplinaCurso to 'professor'@'localhost';
grant select on projetobd2.Disciplina to 'professor'@'localhost';
grant select on projetobd2.Prerequisito to 'professor'@'localhost';
grant select on projetobd2.Departamento to 'professor'@'localhost';
grant select on projetobd2.Turma to 'professor'@'localhost';

grant select on projetobd2.Boletim to 'professor'@'localhost';
grant update on projetobd2.Boletim to 'professor'@'localhost';

grant select on projetobd2.Professor to 'professor'@'localhost';
grant update on projetobd2.Professor to 'professor'@'localhost';

grant select on projetobd2.Usuario to 'aluno'@'localhost';
grant update on projetobd2.Usuario to 'aluno'@'localhost';

-- privilégios de coordenador
grant select on projetobd2.Curso to 'coordenador'@'localhost';
grant update on projetobd2.Curso to 'coordenador'@'localhost';

grant select on projetobd2.DisciplinaCurso to 'coordenador'@'localhost';
grant insert on projetobd2.DisciplinaCurso to 'coordenador'@'localhost';
grant delete on projetobd2.DisciplinaCurso to 'coordenador'@'localhost';
grant update on projetobd2.DisciplinaCurso to 'coordenador'@'localhost';

grant select on projetobd2.Disciplina to 'coordenador'@'localhost';
grant insert on projetobd2.Disciplina to 'coordenador'@'localhost';
grant delete on projetobd2.Disciplina to 'coordenador'@'localhost';
grant update on projetobd2.Disciplina to 'coordenador'@'localhost';

grant select on projetobd2.PreRequisito to 'coordenador'@'localhost';
grant insert on projetobd2.PreRequisito to 'coordenador'@'localhost';
grant delete on projetobd2.PreRequisito to 'coordenador'@'localhost';
grant update on projetobd2.PreRequisito to 'coordenador'@'localhost';

grant select on projetobd2.Departamento to 'professor'@'localhost';
grant update on projetobd2.Departamento to 'professor'@'localhost';

grant select on projetobd2.Turma to 'professor'@'localhost';

grant select on projetobd2.Boletim to 'professor'@'localhost';
grant update on projetobd2.Boletim to 'professor'@'localhost';

grant select on projetobd2.Professor to 'professor'@'localhost';
grant update on projetobd2.Professor to 'professor'@'localhost';

grant select on projetobd2.Usuario to 'aluno'@'localhost';
grant update on projetobd2.Usuario to 'aluno'@'localhost';

flush privileges;