-- limpa todas as tabelas 
SET FOREIGN_KEY_CHECKS = 0; 
truncate table debug;
truncate table Boletim;
truncate table Periodo;
truncate table Turma;
truncate table Historico;
truncate table AlunosInscritos;
truncate table DisciplinaCurso;
truncate table PreRequisito;
truncate table PeriodoInscricao;
truncate table Disciplina;
truncate table Matricula;
truncate table Curso;
truncate table Endereco;
truncate table Aluno;
truncate table Professor;
truncate table Departamento;
truncate table Administrador;
truncate table Usuario;
truncate table Turma;
SET FOREIGN_KEY_CHECKS = 1;



-- administrador padrão
insert into Usuario (primeiroNome, login, senha, tipo) values ('Administrador', 'admin', 'admin', 'Administrador');
insert into Administrador (idAdmin) values (1);

insert into Usuario (primeiroNome, ultimoNome, login, senha, tipo)values
('Tiririca','da Silva','tiririca', 'admin', 'administrador');
insert into Administrador (idAdmin) values (2);


-- administrador padrão / Aluno / professor
insert into Usuario (primeiroNome, ultimoNome, login, senha, tipo) 
values 
('uira','verissimo','uirado','1234','aluno'),
('paulo','Hibernate','paulo','1234','professor'),
('Segio','Silveira','sergio','12345','aluno'),
('pirulito','POP','vermelho','fino','aluno'),
('joão','rego','rego','0','professor'),
('Trolha','','trolha','abcd','professor'),
('Moacir','','moacir','1234','aluno'),
('Nathalia','','nath','0000','aluno'),
('Miguel','','miguel','0000','aluno'),
('Davi','','davi','0000','aluno'),
('Arthur','','art','0000','aluno'),
('Julia','','julia','0000','aluno'),
('Isabella','','isa','0000','aluno'),
('Manuela','','manu','0000','aluno'),
('Laura','','laura','0000','aluno'),
('Gabriel','','gabr','0000','aluno'),
('Rafael','','rafa','0000','aluno'),
('Helena','','hele','0000','aluno'),
('Fernando','','mago','magodapalha','professor'),
('Fletcher','Noble','SGU94ITJ8PD','GRV96WEL5LH','aluno'),
('Justine','Foster','DRS20TIZ8ZP','NMI29RFU2CV','aluno'),
('Brenda','Woodard','LHV77CPJ8DJ','OSD91AMP9ZW','aluno'),
('Matthew','Mack','ULI16XAE2GI','IND98GVK5UW','aluno'),
('Noble','Woodward','QFF38NGG6XH','VLB43LFM1EW','aluno'),
('Aurora','Stuart','ZDI89VIZ2BN','NLR96CYF4OJ','aluno'),
('Ferdinand','Garner','QCO32ZLL2FO','BAJ11OJF3BT','aluno'),
('Byron','Barnett','PZY03GSH6YH','AOQ80PUV1BQ','aluno'),
('Jamal','Clements','CLY55VLC4UZ','MHV08GSI3ZB','aluno'),
('April','Henson','PTM71RRT0RO','BPM67ZIN8MY','aluno'),
('Kyla','Kirkland','PDS91YCE8AW','EZY48JSU1QQ','aluno'),
('Nigel','Dejesus','EBG78KNY0CQ','YSN59RFU8KM','aluno'),
('Brody','Caldwell','ZDY93GAJ4EA','ZNE75DYG4GQ','aluno'),
('Henry','Zimmerman','UBK93DVO4AQ','MUY73XNF0SR','aluno'),
('Amery','Graham','LGI84TQJ2WI','XAI79ROA9GP','aluno'),
('Jescie','Blanchard','WXG36BUM0HH','MCU43MDH8YK','aluno'),
('Aileen','Hunt','ESJ61EOR7HL','JRT90LFN9AM','aluno'),
('Neve','Lester','WWQ93WLE0NC','YGP28FGA4EV','aluno'),
('Baxter','Neal','FET96MSQ1HA','NWU11VXS8OU','aluno'),
('Mary','Daugherty','WQJ08DDP8JR','KTR40RMW3SS','aluno'),
('Connor','Crawford','LMZ93KLT3MH','NMP69GFS3TB','aluno'),
('Odysseus','Tyler','EAF70BPI7GU','NRU29NGE9VN','aluno'),
('Zephr','Flores','VYH80EPL3IT','ATS57UOP0SZ','aluno'),
('Stephanie','Patrick','QHR70ZFG8BD','QHF28NGI0OG','aluno'),
('Jolie','Drake','AMO02XPC3UU','EWA19PMG2CN','aluno'),
('Thane','Decker','DUS23JGJ0TC','HPW73HWL9BQ','aluno'),
('Kathleen','Richards','AUU47WXQ2YD','XFC60TYI7RA','aluno'),('Pearl','Black','UAP23UFV8KH','CFK12BZV5SS','aluno'),('Dean','Jackson','PWV37YRB2IB','NMX09RCV6SW','aluno'),('Gillian','Evans','HRY78STG4PQ','XMT95MIW8IJ','aluno'),('Avram','Chase','IQF67MRJ5TU','BGN68QOL3UH','aluno'),('Mohammad','Mcmillan','QYK95XTS4IQ','IHP82YYA1BV','aluno'),('Kareem','Pickett','QSP32KVR3YC','QCK02RDC1XP','aluno'),('Morgan','Vance','TDD01BOQ9YG','VXU60ADE9CP','aluno'),('Deacon','Talley','HNF43DLQ0FD','LVV53CYO7EQ','aluno'),('Deacon','Martin','TIS80HIF3EP','XEE39QEX0OJ','aluno'),('Slade','Mack','HRS70JSZ1MR','GVB45DRX5UI','aluno'),('Gage','Marsh','RPC03OHO1JW','IWJ32XPB5TL','aluno'),('Benedict','Key','SJN12GOG3EY','WOQ45BJV8SQ','aluno'),('Yvonne','Jennings','PIB24NGO8RA','ZGZ62NUJ2KI','aluno'),('Sydney','Powell','QMF47NOM7DE','SRN05MUR2YI','aluno'),('Steven','Waller','VWG50SYN1WM','CXM29HQM7VV','aluno'),('Kane','Matthews','TLR42KRE4IL','PKP48XZF6RP','aluno'),('Claire','Decker','OYJ56UDB7GT','IRY50QHI2WL','aluno'),('Jocelyn','Rivera','OCT57GQB8YK','SJM15PXQ4DY','aluno'),('Chadwick','Day','IHU45TWB7II','ABR82ALQ3II','aluno'),('Garrison','Lloyd','KMI60OWX3GB','XFX65IJR7MX','aluno'),('Theodore','Ramirez','SQW33XQD5CB','SUX64IXM2RX','aluno'),('Chanda','Valdez','LUZ83ZQR8NZ','RRB05AZG1IN','aluno'),('Vivian','Murphy','NWN18MGX2JH','KKT56NQJ8IS','aluno'),('Alika','Barton','RUH72UAB9XT','BOX84ZFE5CE','aluno'),('Cade','Chavez','KCL83XKH5BW','OWX40ORC7AL','aluno'),('Jillian','Cherry','VNM69JLI0XI','DIR59CKU4TQ','aluno'),('Lysandra','Torres','HZT42XSM3EO','FWW76UBV2LW','aluno'),('Ian','Frost','TBD39OOR7NL','TQO79GNJ5PE','aluno'),('Rhiannon','Chandler','YLG27WVO1YC','TAV71IZV9EZ','aluno'),('Yvonne','Dyer','LAT57DBZ8SE','PHF59BJL0ZU','aluno'),('Xyla','Barlow','YKG32FRH9PB','TTT22SLD0IJ','aluno'),('Shelly','Hutchinson','EPS80QSZ2ER','ZVU87VMH4NE','aluno'),('Octavius','Sweeney','MMH28AOA3UD','BNJ69KVQ9EZ','aluno'),('Beck','Berger','DEE33EYD0SX','GUM09PLR9CK','aluno'),('Dai','Shannon','GRN00YWY5OJ','UDT11VWU8KT','aluno'),('Cheyenne','Hobbs','PSF16WID9BT','NSW93BIL4KF','aluno'),('Sara','Gilbert','YAW79JHV8AG','IYV11SDK8NV','aluno'),('Plato','Pittman','IYU23OYN2SO','ZXX22TCA5OI','aluno'),('Harper','Long','ZXM38IKR9GD','IBK68FON0UZ','aluno'),('Kuame','Carpenter','KZY76JSH7IJ','VRA87SGT5CK','aluno'),('Kelly','Calhoun','HVD92ODT0TV','ESW12BKI8YL','aluno'),('Stone','Fry','RXT49DSQ8DN','MDK66NDG0FE','aluno'),('Shelby','Orr','TBS63LVA8BD','LRF09QRH6SH','aluno'),('Jerry','Moody','OAI91THC3XP','FIF72FDE5TA','aluno'),('Dana','Steele','AOT09VRE1UW','ZEP34PIY7TC','aluno'),('Quon','Mitchell','QCZ66NVW3LI','RUN77KFS1EA','aluno'),('Morgan','Kane','NTR43IFG7GQ','RVW45BJH4FF','aluno'),('William','Ramsey','IUG80AMP4QJ','UNU36BAZ6UV','aluno'),('Lee','Brewer','HRN42VCC0EN','CUH41PRK2UR','aluno'),('Colleen','Robbins','SLX87LYN6UT','XLT99NHF2WP','aluno'),('Jared','Barr','UTI96RQJ0LF','YBA48UGP2JQ','aluno'),('Noel','Chambers','XKD29UVH5ZR','KGB36YXY5MN','aluno'),('Fuller','Browning','QTV16VXT3LK','XBD49XPP2EJ','aluno'),('Anthony','Bell','HPB11GOR7LJ','VFI96JDI7SI','aluno'),('Jacqueline','Sweeney','QDA45NAN9ZQ','SOO04MGQ9DN','aluno'),('Germaine','Golden','GKB42WPV9BW','NDO10INC1ZY','aluno'),('Kaye','Holcomb','UBZ94NOZ4NZ','MPS68FFR7SG','aluno'),('Claire','Collins','AXR41CKI1GW','NDE22CPS5AP','aluno'),('Ulysses','Riggs','PAI25YEK5OX','SZN00GJS9NC','aluno'),('Colorado','Cummings','QME34OJD2EO','EYB42YLN1EV','aluno'),('Quamar','Dickson','JTQ54ZZN3BM','EMU08YNQ4SS','aluno'),('Linda','Valentine','WFX18WOL6HZ','HPU60WMF7LD','aluno'),('Brynn','Cortez','FWN93NHJ6IO','JZI94PWT1OZ','aluno'),('Jeanette','Moran','SNQ58XPW0IN','FXO40FAM6RD','aluno'),('Margaret','Donaldson','GVK69XKY8YV','YHU34LQJ9CT','aluno'),('Nehru','Griffin','MOJ86ZFM6FP','JYE16DAD8TS','aluno'),('Imogene','Rowland','PCZ71GFT8TP','QEO42PKK0OX','aluno'),('Jonah','Reed','VCZ74AGF4YD','TKA53RIL1HY','aluno'),('Indira','Ware','CIH12ENU0WI','ENN04FBQ2DR','aluno'),('Jordan','Clayton','NKV45PPQ2NE','NRR23UBU9EM','aluno'),('Hollee','Hughes','JEZ93LLV1VK','TEC07TBL7HT','aluno'),('Madeline','Cleveland','GZL63WGS3KT','VXN79VIR5WB','aluno'),('Steel','Warner','EEG10HLO5NZ','JEP24NVR6QP','aluno');

-- inserindo departamento, com update no idCoord apos o insert da tabela professor
-- ...pois existe uma dependendencia entre eles
insert into Departamento (nome,areaAtuacao) values 
('Departamento homosapiens','humanas'),
('Departamento einstein','exatas'),
('Departamento da Restauracao','saúde');

-- inserindo professor
insert into Professor (idProf,cfe,idDpto,coord) values
(4,90000000001,3,false),
(7,90000000002,3,false),
(8,90000000003,3,true),
(21,90000000004,2,false);

-- inserindo Aluno (A tabela a ordem se refere ao atributo UNIQUE)
insert into Aluno (idAluno,cpf) values 
(3,'09823456782'),
(5,'07736796424'),
(6,'05676523923'),
(9,'17787298712'),
(10,'05543425687'),
(11,'90950000019'),
(12,'04145012097'),
(13,'42094160025'),
(14,'80195326091'),
(15,'96463513016'),
(16,'56812318019'),
(17,'11212312333'),
(18,'11515915999'),
(19,'44545645666'),
(20,'77878978999'),
(22,'30378916197'),(23,'05046949715'),(24,'19619761438'),(25,'22695034374'),(26,'22258176904'),(27,'78251071867'),(28,'85614437548'),(29,'79174460054'),(30,'39251366093'),(31,'03766726217'),(32,'71819024960'),(33,'70794106013'),(34,'30231627335'),(35,'25879091391'),(36,'44322318715'),(37,'13341386070'),(38,'32771205479'),(39,'84992133070'),(40,'97642724365'),(41,'64259904005'),(42,'72224120053'),(43,'62727851273'),(44,'96889290968'),(45,'26644321004'),(46,'72022111803'),(47,'43380918567'),(48,'03980386830'),(49,'51788699325'),(50,'39387321166'),(51,'71594144310'),(52,'44154669192'),(53,'14253849958'),(54,'81678950832'),(55,'34887764593'),(56,'58054751686'),(57,'44505417057'),(58,'17591683485'),(59,'17450164101'),(60,'46981996993'),(61,'17657794873'),(62,'70201428106'),(63,'60611604877'),(64,'37756922991'),(65,'32927305939'),(66,'09923685747'),(67,'74770309710'),(68,'42915482523'),(69,'86443043067'),(70,'20106957109'),(71,'37936293081'),(72,'43528517183'),(73,'08532314607'),(74,'61088061610'),(75,'56182122408'),(76,'10314125301'),(77,'00860498510'),(78,'63154753576'),(79,'27195234150'),(80,'98992930116'),(81,'32338262191'),(82,'02648301442'),(83,'10871838063'),(84,'72311464628'),(85,'48979683801'),(86,'49397641607'),(87,'19050233263'),(88,'17842132380'),(89,'37054506415'),(90,'34573296472'),(91,'48746001583'),(92,'50456782155'),(93,'83892448939'),(94,'00454541043'),(95,'30835198038'),(96,'45247866274'),(97,'02638155933'),(98,'90864901194'),(99,'60778730240'),(100,'80684641137'),(101,'07344301000'),(102,'73484454121'),(103,'18798026506'),(104,'92644814329'),(105,'76829225810'),(106,'46065053881'),(107,'68376100918'),(108,'40393754302'),(109,'92299487733'),(110,'03859508222'),(111,'15780135034'),(112,'83042662152'),(113,'92446601976'),(114,'72171859028'),(115,'41417501810'),(116,'17724488369'),(117,'12760056512'),(118,'57746079678'),(119,'56124947059'),(120,'14740704781'),(121,'05500356442');

insert into Endereco (idAluno, logradouro, numero, complemento, bairro, cidade, estado) values
(3,'Rua do pirarucu','25','casa','Areias','Recife','PE'),
(5,'Av.Das andorinhas','12','Ap. 203','casa amarela','Recife','PE'),
(6,'Rua 13','93','casa','vasco da gama','Recife','PE'),
(9,'Av.pan-nordestina','1926','casa terreo','ouro preto','olinda','PE'),
(10,'Av.Getulio vargas','s/n','casa terreo','casa caiada','olinda','PE'),
(11,'Av.pan-nordestina','1926','casa terreo','ouro preto','olinda','PE'),
(12,'Av.pan-nordestina','1926','casa terreo','ouro preto','olinda','PE'),
(13,'Av.pan-nordestina','1926','casa terreo','ouro preto','olinda','PE'),
(14,'Av.pan-nordestina','1926','casa terreo','ouro preto','olinda','PE'),
(15,'Av.pan-nordestina','1926','casa terreo','ouro preto','olinda','PE'),
(16,'Av.pan-nordestina','1926','casa terreo','ouro preto','olinda','PE'),
(17,'Av.pan-nordestina','1926','casa terreo','ouro preto','olinda','PE'),
(18,'Av.pan-nordestina','1926','casa terreo','ouro preto','olinda','PE'),
(19,'Av.pan-nordestina','1926','casa terreo','ouro preto','olinda','PE'),
(20,'Av.pan-nordestina','1926','casa terreo','ouro preto','olinda','PE');


-- inserindo curso
insert into Curso (nome,cargaHorariaMinima,creditos,idDpto) values 
('Ciência da Computação',2000,160,1),
('Engenharia Civil',2020,180,2),
('Arquitetura',2020,180,2),
('Direito',2020,180,2),
('Medicina',3200,300,3);

-- inserindo Matricula
insert into Matricula (idCurso, idAluno, status, tipoAdmissao) values
(1,3,'ativa','vestibular'),
(2,5,'ativa','vestibular'),
(3,6,'cancelada','transferencia'),
(1,9,'trancada','transferencia'),
(1,10,'concluida','vestibular'),
(1,11,'ativa','transferencia'),
(1,12,'ativa','transferencia'),
(1,13,'ativa','transferencia'),
(1,14,'ativa','transferencia'),
(1,15,'ativa','transferencia'),
(1,16,'ativa','transferencia'),
(1,17,'ativa','transferencia'),
(1,18,'ativa','transferencia'),
(1,19,'ativa','transferencia'),
(1,20,'ativa','transferencia'),
(2,22,'ativa','transferencia'),(3,23,'ativa','transferencia'),(1,24,'ativa','transferencia'),(2,25,'trancada','transferencia'),(2,26,'ativa','transferencia'),(1,27,'concluida','vestibular'),(1,28,'ativa','vestibular'),(2,29,'ativa','transferencia'),(2,30,'ativa','transferencia'),(2,31,'ativa','transferencia'),(1,32,'ativa','transferencia'),(2,33,'ativa','transferencia'),(2,34,'ativa','transferencia'),(2,35,'ativa','vestibular'),(3,36,'ativa','transferencia'),(2,37,'ativa','transferencia'),(2,38,'ativa','vestibular'),(2,39,'ativa','vestibular'),(1,40,'ativa','vestibular'),(2,41,'concluida','vestibular'),(2,42,'ativa','vestibular'),(2,43,'ativa','vestibular'),(1,44,'ativa','transferencia'),(1,45,'ativa','vestibular'),(3,46,'ativa','vestibular'),(1,47,'cancelada','transferencia'),(1,48,'trancada','vestibular'),(3,49,'cancelada','vestibular'),(1,50,'ativa','transferencia'),(2,51,'ativa','vestibular'),(1,52,'trancada','transferencia'),(3,53,'ativa','vestibular'),(2,54,'ativa','transferencia'),(3,55,'ativa','vestibular'),(3,56,'cancelada','transferencia'),(3,57,'ativa','vestibular'),(3,58,'ativa','transferencia'),(1,59,'ativa','transferencia'),(2,60,'ativa','vestibular'),(3,61,'ativa','vestibular'),(3,62,'cancelada','transferencia'),(1,63,'ativa','vestibular'),(3,64,'trancada','transferencia'),(2,65,'concluida','transferencia'),(2,66,'ativa','transferencia'),(1,67,'ativa','transferencia'),(3,68,'trancada','transferencia'),(2,69,'concluida','vestibular'),(1,70,'cancelada','vestibular'),(1,71,'ativa','transferencia'),(1,72,'concluida','vestibular'),(2,73,'ativa','vestibular'),(1,74,'ativa','vestibular'),(2,75,'ativa','transferencia'),(3,76,'ativa','transferencia'),(1,77,'concluida','transferencia'),(1,78,'ativa','vestibular'),(1,79,'cancelada','transferencia'),(3,80,'trancada','transferencia'),(1,81,'cancelada','transferencia'),(2,82,'ativa','transferencia'),(1,83,'ativa','vestibular'),(1,84,'ativa','vestibular'),(2,85,'ativa','vestibular'),(3,86,'ativa','transferencia'),(2,87,'trancada','vestibular'),(3,88,'ativa','vestibular'),(2,89,'trancada','transferencia'),(3,90,'trancada','vestibular'),(2,91,'trancada','transferencia'),(1,92,'ativa','vestibular'),(2,93,'cancelada','vestibular'),(2,94,'ativa','transferencia'),(2,95,'ativa','vestibular'),(3,96,'ativa','transferencia'),(1,97,'concluida','transferencia'),(3,98,'ativa','vestibular'),(2,99,'ativa','transferencia'),(2,100,'ativa','vestibular'),(3,101,'concluida','vestibular'),(2,102,'ativa','vestibular'),(2,103,'ativa','vestibular'),(2,104,'cancelada','transferencia'),(1,105,'ativa','vestibular'),(1,106,'concluida','vestibular'),(2,107,'ativa','transferencia'),(1,108,'trancada','vestibular'),(2,109,'ativa','vestibular'),(1,110,'cancelada','transferencia'),(1,111,'trancada','vestibular'),(2,112,'ativa','vestibular'),(2,113,'ativa','transferencia'),(3,114,'cancelada','vestibular'),(2,115,'trancada','transferencia'),(1,116,'cancelada','vestibular'),(3,117,'trancada','transferencia'),(1,118,'ativa','transferencia'),(1,119,'ativa','transferencia'),(3,120,'cancelada','transferencia'),(3,121,'ativa','vestibular');

insert into Disciplina (nome,idDpto,idProf) values 
('Programacao 1',2,21),
('Programação 2',2,21),
('Arq1',3,8),
('Arq2',3,7),
('Engenharia de Software',2,21),
('Linguagens Formais',3,4),
('POO',2,21);

insert into DisciplinaCurso (idCurso, idDisci, periodo, obrigatoriedade, cargaHoraria, creditos) values 
(2,2,5, true,180,60),
(2,1,6, true,180,60),
(3,3,5, true,220,90),
(3,4,6, true,220,90),
(1,1,6, false,500,90),
(1,2,5, true,180,60),
(1,3,5, true,180,60),
(1,4,5, true,180,60),
(1,5,5, true,180,60),
(1,7,7, true,180,60);

insert into PreRequisito (idCurso,idDisci,idDisciPreReq) values (1,2,1), (1,7,1), (1,3,4),(1,4,2);

-- insert into Periodo (idCurso, idAluno, idTurma, data, numero) values 
-- (2,3,1,'20171031',4),(2,5,2,'20170904',3);


call iniciarPeriodoInscricao();

-- inscrever alunos no curso 1
insert into AlunosInscritos (ano,semestre,idAluno,idCurso,idDisci) values
(2017,2,3,1,1),(2017,2,11,1,1),(2017,2,12,1,1),(2017,2,13,1,1),(2017,2,14,1,1),(2017,2,15,1,1),(2017,2,16,1,1),(2017,2,17,1,1),
(2017,2,18,1,1),(2017,2,19,1,1),(2017,2,20,1,1),(2017,2,24,1,1),(2017,2,28,1,1),(2017,2,32,1,1),(2017,2,40,1,1),(2017,2,44,1,1),
(2017,2,45,1,1),(2017,2,50,1,1),(2017,2,59,1,1),(2017,2,63,1,1),(2017,2,67,1,1),(2017,2,71,1,1),(2017,2,74,1,1),(2017,2,78,1,1),
(2017,2,83,1,1),(2017,2,84,1,1),(2017,2,92,1,1),(2017,2,105,1,1),(2017,2,118,1,1),(2017,2,119,1,1),

(2017,2,3,1,2),(2017,2,11,1,2),(2017,2,12,1,2),(2017,2,13,1,3),(2017,2,14,1,3),(2017,2,15,1,3),(2017,2,16,1,3),(2017,2,17,1,3),
(2017,2,18,1,2),(2017,2,19,1,2),(2017,2,20,1,2),(2017,2,24,1,3),(2017,2,28,1,3),(2017,2,32,1,3),(2017,2,40,1,3),(2017,2,44,1,3),
(2017,2,45,1,2),(2017,2,50,1,2),(2017,2,59,1,2),(2017,2,63,1,3),(2017,2,67,1,3),(2017,2,71,1,3),(2017,2,74,1,3),(2017,2,78,1,3),
(2017,2,83,1,2),(2017,2,84,1,2),(2017,2,92,1,2),(2017,2,105,1,3),(2017,2,118,1,3),(2017,2,119,1,3),

(2017,2,3,1,4),(2017,2,11,1,4),(2017,2,12,1,4),(2017,2,13,1,4),(2017,2,14,1,4),(2017,2,15,1,4),(2017,2,16,1,4),(2017,2,17,1,4),
(2017,2,18,1,4);

-- inscrever alunos no curso 2
insert into AlunosInscritos (ano,semestre,idAluno,idCurso,idDisci) values
(2017,2,5,2,2),(2017,2,22,2,1),(2017,2,26,2,2),(2017,2,29,2,2),(2017,2,30,2,1),(2017,2,31,2,2),(2017,2,33,2,2),(2017,2,34,2,1),
(2017,2,35,2,1),(2017,2,37,2,1),(2017,2,38,2,2),(2017,2,39,2,2),(2017,2,42,2,1),(2017,2,43,2,2),(2017,2,51,2,2),(2017,2,54,2,1),
(2017,2,60,2,1),(2017,2,66,2,2),(2017,2,73,2,2),(2017,2,75,2,1),(2017,2,82,2,1),(2017,2,85,2,2),(2017,2,94,2,1),(2017,2,95,2,2),
(2017,2,99,2,2),(2017,2,100,2,1),(2017,2,102,2,1),(2017,2,103,2,2),(2017,2,107,2,2),(2017,2,109,2,2),(2017,2,112,2,2),(2017,2,113,2,2);

-- inscrever alunos no curso 3
insert into AlunosInscritos (ano,semestre,idAluno,idCurso,idDisci) values
(2017,2,23, 3,4),(2017,2,36, 3,4),(2017,2,46, 3,4),(2017,2,53, 3,4),(2017,2,55, 3,4),(2017,2,57, 3,4),(2017,2,58, 3,4),(2017,2,61, 3,4),
(2017,2,76, 3,4),(2017,2,86, 3,4),(2017,2,88, 3,3),(2017,2,96, 3,3),(2017,2,98, 3,3),(2017,2,121, 3,3);

insert into Turma (idDisci,idCurso) values (2,1),(2,1),(2,1),(3,1),(3,1),(4,1);


insert into Boletim (idAluno,idTurma) values (3,1),(5,1);
update Boletim set primeiraNota=8, segundaNota=10, faltas=6 where Boletim.idAluno=3;
update Boletim set primeiraNota=3, segundaNota=3,faltas=18 where Boletim.idAluno=5;
update Boletim set notaProvaFinal =7 where Boletim.idAluno=5;