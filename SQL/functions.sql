use projetobd2;
-- função para media
DELIMITER $
CREATE FUNCTION mediaCaderneta (n1 FLOAT, n2 FLOAT)  RETURNS FLOAT
BEGIN
declare med float;
set med=(n1+n2)/2;
return med;
END$
DELIMITER ;