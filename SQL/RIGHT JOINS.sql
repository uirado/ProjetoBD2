-- RIGHT JOIN´S

-- exibir nome do departamento e nome completo dos professores que não estão vinculados a nenhum disciplina.
select d.nome, concat(u.primeiroNome, " " ,coalesce(u.ultimoNome, '')) as NomeProfessor, u.id as idProfessor
from Usuario u
inner join Professor p on (u.id = p.idProf)
inner join Departamento d on (p.idDpto = d.idDpto)
where u.id in (select p.idProf from Disciplina d right outer join Professor p on d.idProf=p.idProf where d.idProf is null);


-- Disciplinas que não estão sendo lecionadas por nenhum professor
select distinct d.nome as Disciplinas  from Turma t right join 
DisciplinaCurso dc on t.idDisci = dc.idDisci inner join
Disciplina d on d.idDisci= dc.idDisci where t.idTurma is null;


-- Disciplinas que são pre requisitos para mais de 1 "Cadeira" 
select distinct d.idDisci as idDisciplina, d.nome as nomeDisciplinaPreRequisito from PreRequisito pR right join 
Disciplina d on d.idDisci = pR.idDisciPreReq  where idDisciPreReq is not null  having count(idDisciPreReq) > 1;


-- Listar Nomes dos cursos e quantidade de disciplinas que não possuem pré requisito.
select c.nome, count(d.idDisci) as qtdDisciSemPreReq from Curso c
inner join
(select dc.idCurso, dc.idDisci from PreRequisito pr right join DisciplinaCurso dc
on (dc.idCurso = pr.idCurso and dc.idDisci = pr.idDisci) where pr.idCurso is null) as d
on (c.idCurso = d.idCurso)
group by c.nome;


-- Disciplinas "Pré-Requisitadas" de uma disciplina Pré Requisito
select  pR.idDisciPreReq as preRequisito, d.nome as DisciplinasDependentes from PreRequisito pR right join 
Disciplina d on pR.idDisci=d.idDisci where idDisciPreReq is not null;

