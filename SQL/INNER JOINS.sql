-- 1 Listar nome das disciplinas que tem turmas criadas, a quantidade e quantos alunos inscritos no total, incluindo todos os cursos

select distinct d.nome as nomeDisciplina, d.qtdTurmas, t.totalAlunos from

(select d.idDisci, d.nome, count(t.idTurma) as qtdTurmas
from Turma t, Disciplina d where t.idDisci = d.idDisci
group by d.idDisci)
as d

inner join

(select t.idDisci, count(b.idTurma) as totalAlunos
from Boletim b, Turma t where b.idTurma = t.idTurma
group by t.idDisci)
as t

where d.idDisci = t.idDisci;


-- 2 Listar Cursos e suas disciplinas que tiveram inscrições mas não formaram turmas, mostrando o total de inscritos
select c.nome as nomeCurso, d.nome as nomeDisciplina, count(i.idAluno) as totalInscritos
from Curso c, Disciplina d, AlunosInscritos i
where c.idCurso = i.idCurso and i.idDisci = d.idDisci group by i.idCurso, i.idDisci;


-- 2 Listar as turmas em que todos os alunos passaram por media 
select codigo, idAluno from Turma inner join Boletim on Turma.idTurma=Boletim.idTurma where 
situacao='aprovado por media';


-- Listar PROFESSORES VINCULADOS EM MAIS DE UMA DISCIPLINA
select count(*) as Disciplina ,idProf FROM Disciplina GROUP BY idProf HAVING COUNT(*) > 1;