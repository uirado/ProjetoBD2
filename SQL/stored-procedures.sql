-- stored procedure pra criar as turmas. (distribuir os alunos inscritos nas disciplinas)

DROP PROCEDURE IF EXISTS getIncricoesAtuais;
DROP PROCEDURE IF EXISTS alunosInscritosDisciplinasCurso;
DROP PROCEDURE IF EXISTS contarHorasDisci;
DROP PROCEDURE IF EXISTS atualizaCurso;
DROP PROCEDURE IF EXISTS iniciarPeriodoInscricao;
DROP PROCEDURE IF EXISTS encerrarPeriodoInscricao;
-- procedure para iniciar o periodo de matricula
DELIMITER $
CREATE PROCEDURE iniciarPeriodoInscricao()
BEGIN
	-- verificar se já existe um periodo ativo
    set @ativo = (select count(*) > 0 from PeriodoInscricao where status = 'ativo');
    if(@ativo) then
		signal sqlstate '45000' set message_text = 'Já existe um período de inscrição ativo';
	else
		set @dataAtual = curdate();
		set @ano = year(@dataAtual);
        set @semestre = ceil(month(@dataAtual)/6);
		insert into PeriodoInscricao (ano, semestre, status) values (@ano, @semestre, 'ativo');
        
        -- altera os privilégios de aluno à tabela AlunosInscritos
        grant insert on projetobd2.AlunosInscritos to 'aluno'@'localhost';
		grant select on projetobd2.AlunosInscritos to 'aluno'@'localhost';
		grant update on projetobd2.AlunosInscritos to 'aluno'@'localhost';
		grant delete on projetobd2.AlunosInscritos to 'aluno'@'localhost';
    end if ;
END$
DELIMITER ;


-- procedure para encerrar periodo de inscrição
DELIMITER $
CREATE PROCEDURE encerrarPeriodoInscricao()
BEGIN
	-- verificar se existe um periodo ativo
    set @ativo = (select count(*) > 0 from PeriodoInscricao where status = 'ativo');
    if(@ativo = 0) then
		signal sqlstate '45000' set message_text = 'Não existe periodo de inscrição ativo';
	else
		-- altera os privilégios de aluno à tabela AlunosInscritos
        revoke insert on projetobd2.AlunosInscritos from 'aluno'@'localhost';
		revoke select on projetobd2.AlunosInscritos from 'aluno'@'localhost';
		revoke update on projetobd2.AlunosInscritos from 'aluno'@'localhost';
		revoke delete on projetobd2.AlunosInscritos from 'aluno'@'localhost';
        
        call verificarTrancamentos();
        -- call verificarConclusoes;
        
        update PeriodoInscricao set status = 'encerrado' where status = 'ativo';
    end if ;
END$
DELIMITER ;


-- ESTA PROCEDURE RETORNA CURSOS E DISCIPLINAS COM ALUNOS INSCRITOS do periodo de inscrição atual
DELIMITER $
CREATE PROCEDURE getIncricoesAtuais()
BEGIN
	set @anoAtual = (select ano from PeriodoInscricao where status = 'ativo');
    set @semestreAtual = (select semestre from PeriodoInscricao where status = 'ativo');
	select distinct ano, semestre, idCurso, idDisci from AlunosInscritos where ano = @anoAtual and semestre = @semestreAtual;
END$
DELIMITER ;


-- ESTA PROCEDURE ESTÁ BUSCANDO TODOS OS ALUNOS INSCRITOS PARA UMA DETERMINADA DISCIPLINA DE UM MESMO CURSO!!
DELIMITER $
CREATE PROCEDURE alunosInscritosDisciplinasCurso (IN ano INT, IN semestre INT, IN idCurso int, IN idDisci int)
BEGIN
select * from AlunosInscritos i where i.ano = ano and i.semestre = semestre and i.idCurso = idCurso and i.idDisci = idDisci;
END$
DELIMITER ;


-- procedure para contar o total de carga horaria de um curso, somando as horas das disciplinas, separando entre obrigatoria ou não obrigatoria
DELIMITER $
create procedure contarHorasDisci(IN idCurso int, IN obrig bool, OUT result int)
begin
	set result = (select coalesce(sum(d.cargaHoraria), 0) from Curso c, DisciplinaCurso d
					where c.idCurso = idCurso and c.idCurso = d.idCurso and d.obrigatoriedade = obrig);
end $
DELIMITER ;


-- faz a atualização e checagem das horas obrigatorias e não obrigatorias com a carga horaria minima para atualizar o status
DELIMITER $
create procedure atualizaCurso(IN idCurso int)
begin
	call contarHorasDisci(idCurso, true, @horasObrigatorias);
    call contarHorasDisci(idCurso, false, @horasNaoObrigatorias);
    set @cargaHorariaMinima = (select cargaHorariaMinima from Curso c where c.idCurso = idCurso);
    set @cargaHorariaTotal = @horasObrigatorias + @horasNaoObrigatorias;
    
    if (@cargaHorariaTotal < @cargaHorariaMinima) then
		set @status = 'Horas insuficientes';
	elseif(@horasObrigatorias > @cargaHorariaMinima) then
        signal sqlstate '45000' set message_text = 'A disciplina excede as horas obrigatórias exigidas pelo curso';
    else	
		set @status = 'Disponível';
    end if;
    
    update Curso c set c.horasObrigatorias = @horasObrigatorias, c.status = @status, c.cargaHorariaTotal = @cargaHorariaTotal
		where c.idCurso = idCurso;
end $
DELIMITER ;


DELIMITER $
create procedure verificarTrancamentos()
    begin
		-- tranca todas as matrículas que não tiver períodos, ou seja, que não entrou em nenhuma turma 
		update Matricula m left join Periodo p 
        on (m.idCurso = p.idCurso and m.idAluno = p.idAluno)
        set m.status = 'trancada'
        where m.status = 'ativa' and p.idCurso is null and p.idAluno is null and p.idTurma is null;
    end $
DELIMITER ;

