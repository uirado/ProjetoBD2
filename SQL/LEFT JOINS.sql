-- Disciplinas que não possuem Pré Requisitos
select  nome  from Disciplina d left join 
PreRequisito pR on pR.idDisci=d.idDisci where idCurso is null;
 
-- Disciplinas que não houveram inscritos
select d.nome from Disciplina d inner join DisciplinaCurso dc on d.idDisci = dc.idDisci
left join AlunosInscritos al on (dc.idDisci=al.idDisci and dc.idCurso = al.idCurso)
where al.idAluno is null;
 
-- Departamento administrando ZERO disciplinas
select de.nome, areaAtuacao from Departamento de left join 
Disciplina d on d.idDpto= de.idDpto where d.idDisci is null;
 

 
 



